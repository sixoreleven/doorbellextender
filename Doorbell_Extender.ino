#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

/* RF24 to (Arduino) Connections
 *     [[ Antenna ]] 
 * 
 * VCC   CS    MOSI  IRQ
 *(3.3v) (9)  (11)   (-)
 *  o     o     o     o 
 *  
 *  o     o     o     o
 * GND   CE    SCK   MISO
 *(GND) (10)  (13)   (12)
 *
 */


// CE = pin 9/4, CSN = pin 10/5 
// Arduino
RF24 radio(9,10);

// Leave arduino pin A0 open to be TX, or connect to GND to be RX.
// Comment out for other usages. (Like what? idk)
const int NODE_ROLE = A0;

// Tx Signals! (Pin, and size of signal to send. Called "Bell" because it's for my Doorbell.
const uint8_t BELL_PIN[] = { 7 };
const uint8_t SIGNAL_SIZE = sizeof(BELL_PIN);

#define TX_LED    13
#define LED_BLUE  7
#define LED_GREEN 6
#define LED_RED   3
#define NUM_LEDS  3

const int LED_PINS[] = { LED_RED, LED_BLUE, LED_GREEN };
bool lightsAreOn = false;
const int TIMES_TO_CYCLE = 6;

// Single radio pipe address for the 2 nodes to communicate.
const int pipe = 0xE8E8F0F0E1LL;

// Different roles of the Node. 
typedef enum { INVALID, TX_NODE = 1, RX_NODE = 2 } ROLE_ENUM;

// Easier-to-read roles
const char* ROLE_NAME[] = { "invalid", "TX Node", "RX Node" };

// Role of current Node (Current running sketch)
ROLE_ENUM node_type;

// States of LEDs and Signal.
int bell_state[SIGNAL_SIZE];
//int led_states[NUM_LED_PINS - 1 ];

//int dbg_ledStates = sizeof(led_states);

void LEDsOff();

void setup()
{
  // Defining the role of the Node (Tx or Rx)
  pinMode(NODE_ROLE, INPUT);
  digitalWrite(NODE_ROLE, HIGH);
  delay(20); // Just to get a solid reading on the role pin.

  // Read the address pin, establish the role of the Node
  if ( digitalRead(NODE_ROLE) ) {
    node_type = TX_NODE;
  }
  else {
    node_type = RX_NODE;
  }

  Serial.begin(115200);
//  try {
//    printf_begin();
//  } catch (e) {
//    Serial.print("Error in printf_begin(): ");
//    Serial.println(e);
//  }
  
  Serial.println("Trying radio using the RF24 module");

  Serial.print("This node is a: ");
  Serial.println(ROLE_NAME[node_type]);

  radio.begin();
  delay(3); // begin() actually has a built-in delay of 5ms, so this is redundant.

  // One node TX, the other RX on this "pipe".
  if ( node_type == TX_NODE ) {
    Serial.print("Signal size is: ");
    Serial.println(SIGNAL_SIZE);
    radio.openWritingPipe(pipe);
    pinMode(TX_LED, OUTPUT);
    digitalWrite(TX_LED, HIGH);
    delay(256);
    digitalWrite(TX_LED, LOW);
  }
  else {
    Serial.print("Number of LEDs: ");
    Serial.println(NUM_LEDS);
    radio.openReadingPipe(1,pipe);
    radio.startListening(); // Moved this up from the 'if' below.
    Serial.print("Opening pipe for reading, now listening on channel");
    Serial.println(radio.getChannel());
  }

  if ( node_type == RX_NODE ) {
    radio.startListening();
  }

  // Print all the details I might care about at some point.
  Serial.print("radio details: ");
  radio.printDetails();

  Serial.print("Channel: ");
  Serial.println(radio.getChannel());
  bool bigSigs = radio.testRPD();
  Serial.print("Signals greater than -64dBm on this Channel: ");
  if ( bigSigs == true ) {
    Serial.println("true.");
  }
  else {
    Serial.println("false.");
  }

  if ( bigSigs == true ) {
    Serial.println("Changing channel...");
    Serial.println("(Not actually implemented yet...)");
    ///todo: this.
    
  }

  // Turn LEDs on/high until we start getting keys.
  if ( node_type == RX_NODE ) {
    int i = 0; //NUM_LED_PINS;
    while ( i < NUM_LEDS ) {
//      pinMode(LED_PINS[i], OUTPUT);
//      led_states[i] = HIGH;
//      digitalWrite(LED_PINS[i], led_states[i]);
//      delay(128);
//      led_states[i] = LOW;
//      digitalWrite(LED_PINS[i], led_states[i]);
      //This could also be...
      pinMode(LED_PINS[i], OUTPUT);
      digitalWrite(LED_PINS[i], HIGH);
      delay(255);
      digitalWrite(LED_PINS[i], LOW);
      i++;
    }
  }
  // Set pull-up resistors for all buttons.
  if ( node_type == TX_NODE ) {
    pinMode(BELL_PIN[0] , INPUT);
    digitalWrite(BELL_PIN[0], HIGH); // Why high? - like a CAN, high until pulled low.
  }

  if ( lightsAreOn ) {
    LEDsOff();
  }
} // End Setup()



void loop()
{
  // Check states

  // If in TX mode...
  if ( node_type == TX_NODE ) {
    // Get current state of button and test if current state is different from last state sent.
    bool isDifferent = false;
    int i = SIGNAL_SIZE;
//    while( i > 0 ) {
//    int state = ! digitalRead(BELL_PIN[0]);
//    if ( state != bell_state[0]) 
    int state = digitalRead(BELL_PIN[0]);
    if ( state != bell_state[0]) {
      Serial.println("Change detected! (Button press or whatever)");
      isDifferent = true;
      bell_state[i] = state;
    }
//    }
    // Send the state of buttons to the RX node.
    if ( isDifferent ) {
      Serial.println("Sending...");
      bool dataSent = radio.write( bell_state, SIGNAL_SIZE ); // data to be sent, size of data sent
      if (dataSent) {
        Serial.println("Success!");
        int idx = 0;
        int repetitions = 16;
        for (idx = 0; idx < repetitions; idx++) {
          digitalWrite(TX_LED, HIGH);
          delay(16);
          digitalWrite(TX_LED, LOW); 
        }
      }
      else {
        Serial.println("Fuckin' FAILED!");
      }
    }
    // Try again after short delay.
    delay(20);
  }


  // If in RX mode... recieve state of all buttons, LEDs will mirror button states.
  if ( node_type == RX_NODE ) {
    // If there is data ready.
    if ( radio.available() ) {
//      bool done = false;
//      // Dump the payload until all is received.
//      while ( !done ) {
//        // Fetch payload, see if it was last one.
//        done = radio.read( bell_state, SIGNAL_SIZE );

        while (radio.available()) {
          Serial.println("Receiving data...");
          radio.read( bell_state, SIGNAL_SIZE );
        }
        // Tell us about it.
        Serial.print("Signal received!");

        // When the signal is received, loop through 'j' number of LEDs 'i' number of times.
        int i = 0;
        int j = 0; 
        Serial.print("Times to cycle: ");
        Serial.println(TIMES_TO_CYCLE);
        for (i = 0; i < TIMES_TO_CYCLE; i++) {
          for (j = 0; j < NUM_LEDS; j++) {
            digitalWrite(LED_PINS[j], HIGH);
            delay(75);
            digitalWrite(LED_PINS[j], LOW);
            delay(75);
          }

        }
//        while( i < TIMES_TO_CYCLE ) 
//        {
//          while ( j < NUM_LED_PINS ) 
//          {
//            digitalWrite(LED_PINS[j], HIGH);
//            delay(102);
//            digitalWrite(LED_PINS[j], LOW);
//            delay(102);
//            j++;
//          }
//          i++;
//        }

//
//        while(i--) {      // LED loop
//          if ( button_state[0] ) {
//            led_states[i] ^= HIGH;
//            digitalWrite(LED_PINS[i], led_states[i]);
//            if ( led_states[i] == HIGH ) {
//              LEDsAreOn = true;
//            }
//          }
//        } //End while(i--) // LED loop


//      } // End while (!done)
    } // End if radio.available()

    // Turn off all the LEDs
    if ( lightsAreOn ) {
      LEDsOff();
    }
  } // End if node_type == RX_NODE
} // End loop()


void LEDsOff() {
  delay(127);
  int i = 0;
  while (i < NUM_LEDS ) {
//    led_states[i] = LOW;
    digitalWrite(LED_PINS[i], LOW);
//    digitalWrite(LED_PINS[i], led_states[i]);
    i++;
  }
}
